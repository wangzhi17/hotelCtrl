package com.hotelctrl;


import com.github.yamill.orientation.OrientationPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.peel.react.TcpSocketsModule;
import com.facebook.react.ReactPackage;
import com.reactnativenavigation.NavigationApplication;
import com.reactcommunity.rnlanguages.RNLanguagesPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.tradle.react.UdpSocketsModule;

import java.util.Arrays;
import java.util.List;

import com.rnfs.RNFSPackage;

public class MainApplication extends NavigationApplication {
    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    public boolean isDebug() {
        // Make sure you are using BuildConfig from your own application
        return BuildConfig.DEBUG;
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
                new TcpSocketsModule(),
                new UdpSocketsModule(),
                new BlurViewPackage(),
                new RNSpinkitPackage(),
                new RNI18nPackage(),
                new RNMainReactPackage(),
                new OrientationPackage(),
                new VectorIconsPackage(),
                new RNLanguagesPackage(),
                new FullScreenModule(),
                new RNFSPackage()
        );
    }

    @Override
    public String getJSMainModuleName() {
        return "index";
    }

}
