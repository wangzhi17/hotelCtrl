import {Navigation} from 'react-native-navigation';
import LightCtrl from './src/routers/light/index';
import AirCtrl from './src/routers/air/index';
import TcpCtrl from './src/routers/tcp/index';
import configureStore from './src/store/configureStore';
import {Provider} from 'react-redux';
import Orientation from "react-native-orientation";

export const store = configureStore();
global.tcp = {};
global.downloadFileIP = '192.168.1.20';
Orientation.lockToLandscape();
Navigation.registerComponent('Light', () => LightCtrl, store, Provider);
Navigation.registerComponent('Air', () => AirCtrl, store, Provider);
Navigation.registerComponent('Tcp', () => TcpCtrl, store, Provider);
