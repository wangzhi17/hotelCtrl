import writeLog from "./writeLog";

const net = require('net');
import {updateState, getAllState} from '../store/actions'
import {store} from '../../index'
import stringBuffer from './stringBuffer'


var stringBuffers = new stringBuffer();
export default (ip, port) => {
    global.tcp.client = net.createConnection(port, ip);
    global.tcp.client.on('error', function (error) {
        global.tcp.status = false;

        this.timer && clearInterval(this.timer);
        //console.info("client error控制器掉线的原因:", error);

    });

    global.tcp.client.on('data', function (data) {

        let rcuData = data.toString();
        //console.log('收到的数据', rcuData);
        for (; ;) {
            let handleData = rcuData.substring(0, rcuData.indexOf('};') + 4);
            //console.log('设备状态：', handleData);
            store.dispatch(updateState(handleData));
            writeLog(handleData);
            if (handleData.length >= rcuData.length)
                break;
            else
                rcuData = rcuData.substring(rcuData.indexOf('};') + 4);
        }

    });

    const data = `SET;000000FF;{254.251.0.1};\x0d\x0a`;
    global.tcp.client.on('connect', function (params) {
        global.tcp.status = true;

        global.tcp.client.write(data);
        writeLog(data);

        store.dispatch(getAllState());
        this.timer = setInterval(
            () => {
                if (global.tcp.status) {
                    global.tcp.client.write(data);
                    writeLog(data);
                }
            }, 60000)
    });

    global.tcp.client.on('end', function (params) {
        global.tcp.status = false;
        this.timer && clearInterval(this.timer)
        //console.info("end控制器掉线原因:", params);

    })
}

export function disconnectTcp() {
    this.timer && clearInterval(this.timer);
    global.tcp.client.destroy();
}