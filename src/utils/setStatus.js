import writeLog from "./writeLog";

export default function (lightName, value) {
    let lights = global.config.device[lightName];
    let statusKey = `${lights.lid}.${lights.rcuid}.${lights.port}`;
    let data = `SET;0000000${value};{${global.tcp.ip.split('.')[3]}.${statusKey}};\x0d\x0a`;
    global.tcp.client.write(data);
    writeLog(query);
}