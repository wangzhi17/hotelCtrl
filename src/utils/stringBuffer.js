export default function StringBuffer(str) {
    var arr = [];
    str = str || "";
    arr.push(str);
    this.append = function (str1) {
        arr.push(str1);
        return this;
    };
    this.toString = function () {
        return arr.join("");
    };
};