const RNFS = require('react-native-fs');
const path = RNFS.DocumentDirectoryPath;
const filePath = path + '/config.json';

export default async function (data) {
    let getTime = new Date().getTime();
    let time = new Date(getTime + 8 * 3600 * 1000); // 增加8小时
    let datetime = time.toJSON().substr(0, 23).replace('T', ' ');
    let content = {"type": "log", "data": data, "time": datetime};
    let jsonContent = JSON.stringify(content);

    if (global.tcp.statusIOS) {
        global.tcp.clientIOS.write(jsonContent);
    } else {

        let isExits = RNFS.exists(path);
        let fileIsExits = await RNFS.exists(filePath);
        if (!isExits) {
            await RNFS.mkdir(path);
        }
        if (fileIsExits) {
            console.debug(`写入文件内容：${jsonContent}`);
            await RNFS.appendFile(filePath, jsonContent, 'utf8')
                .then(success => {
                    console.debug('写入Log完成！');
                })
                .catch(error => {
                    console.debug('写入Log失败！');
                })
        }
    }
}