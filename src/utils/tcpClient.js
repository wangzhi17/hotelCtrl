const net = require('net');
import setStatus from './setStatus'
import getStatus from './readStatus'

export default (port) => {
    global.tcp.clientIOS = net.createConnection(port, '127.0.0.1');

    global.tcp.clientIOS.on('error', function (error) {
        global.tcp.statusIOS = false;
        //console.info("clientIOS error掉线的原因:", error);

    });

    global.tcp.clientIOS.on('data', function (data) {
        //与原生端通信端口
        let content = JSON.parse(data.toString());
        let type = content.type;
        if (type === 'set') {
            setStatus(content.name, content.value);
        } else if (type === 'get') {
            getStatus("MakeupRoom");
            getStatus("DoNotDisturb");
        }
    });

    global.tcp.clientIOS.on('connect', function (params) {
        global.tcp.statusIOS = true;
    });

    global.tcp.clientIOS.on('end', function (params) {
        global.tcp.statusIOS = false;
    })
}

export function disconnectTCPIOS() {
    global.tcp.clientIOS.destroy()
}