const RNFS = require('react-native-fs');

export default async function (){
    let filePath = RNFS.DocumentDirectoryPath + '/config.json';

    let isExits = await RNFS.exists(filePath);
    if (isExits){
        let config = await RNFS.readFile(filePath);

        global.tcp.Downfileclient.write(`{"code":0,"msg":${config}`);
        console.info('upload Log文件完成：',config);
    } else {
        global.tcp.Downfileclient.write(`{"code":400,"msg":"文件不存在！"}`);
        console.info('Log 文件不存在！');
    }
}