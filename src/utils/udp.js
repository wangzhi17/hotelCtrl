/**
 * Created by kukuchong on 2018/6/23.
 */

global.Buffer = global.Buffer || require('buffer').Buffer;
import ConnTcp from './tcp'


const dgram = require('dgram');
let udpServer = null;
let udpStatus = null;

export function search(port) {

}

export default (port) => {

    let timeOutHandle = null;
    let timeNum = 0;
    udpServer = dgram.createSocket('udp4');


    udpServer.bind(57246, err => {
        if (err) throw err;
        udpServer.setBroadcast(true)
    })

    function sendUDP(buf) {
        udpServer.send(buf, 0, buf.length, port, `${global.config['gwIP']}`, error => {
            timeOutHandle = setTimeout(() => {
                if (!global.tcp.ip && timeNum > 9) {
                    udpServer.close()
                    clearTimeout(timeOutHandle)

                } else {
                    timeNum++;
                    sendUDP(buf)
                }
            }, 4000)
            if (error) {
                udpStatus = false
                udpServer.close()
            }
        })
    }

    udpServer.once('listening', function () {

        let buf = global.Buffer.from('search all');
        sendUDP(buf)

    })

    udpServer.on('error', err => {
        console.log(`服务器异常：\n${err.stack}`);
        clearTimeout(timeOutHandle);
        udpServer.close()
    })
    udpServer.on('message', (msg, rinfo) => {
        //console.log(`服务器收到：${msg} 来自 ${rinfo.address}:${rinfo.port}`);
        let data = msg.toString();
        //console.info('mac is %s', global.config.gwmac)

        if (global.config.gwmac === data.match(/\s*devMac=([\S]+)\s/)[1]) {
            clearTimeout(timeOutHandle);
            global.tcp.ip = data.match(/\s*devIP=([\S]+)\s/)[1];
            ConnTcp(global.tcp.ip, 6003);//连接控制器
            udpServer.close();
        }

    })
}