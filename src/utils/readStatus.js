import writeLog from "./writeLog";

export default function (lightName) {
    let lights = global.config.device[lightName];
    let statusKey = `${lights.lid}.${lights.rcuid}.${lights.port}`;
    let query = `GET;{${global.tcp.ip.split('.')[3]}.${statusKey}};\x0d\x0a`;
    global.tcp.client.write(query);
    writeLog(query);
}