import {store} from "../../index";
import {initState} from "../store/actions";
import searchUdp from "./udp";
import tcpClientIOS from './tcpClient'
import tcpLog from "./tcpLog";
import writeLog from "./writeLog";

export default async function (isFirst, config) {
    try {
        if (isFirst) {
            try {
                global.config = config;
                store.dispatch(initState(config));
                searchUdp(6002);//搜索网关
                tcpClientIOS(61000);//连接原生端
                //tcpLog('192.168.1.20',60000);
            }catch (e) {

            }

        }
    } catch (err) {
        console.error(err.message);
        await writeLog(err.messages);
    }

}