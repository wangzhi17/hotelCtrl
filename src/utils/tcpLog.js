const net = require('net');
import stringBuffer from './stringBuffer'
import readConfigFile from './readLog'

var stringBuffers = new stringBuffer();

export default (ip, port) => {
    global.tcp.Downfileclient = net.createConnection(port, ip);
    global.tcp.Downfileclient.on('error', function (error) {
        global.tcp.Downfileclient.status = false;
    });
    global.tcp.Downfileclient.on('data', function (data) {

        stringBuffers.append(data.toString());

        let content = JSON.parse(stringBuffers.toString());

        switch (content.type) {
            case 'delete'://删除配置文件
                console.info('delete配置文件');
                break;
            case 'download'://下载配置文件
                console.info('download配置文件');

                break;
            case 'upload'://上传配置文件内容
                console.info('upload Log文件');
                readConfigFile();
                break;
            case 'modify':
                console.info('modify配置文件');

                break;
        }
        stringBuffers.clean();

    });

    global.tcp.Downfileclient.on('connect', function () {
        global.tcp.Downfileclient.status = true;
    });

    global.tcp.Downfileclient.on('end', function () {
        global.tcp.Downfileclient.status = false;
    })
}