/**
 * Created by kukuchong on 2018/6/13.
 */

import en from './locales/en';
import zh_CHT from './locales/zh-CHT'
import zh from './locales/zh'
import I18n,{ getLanguages } from 'react-native-i18n'
import DataRepository from '../utils/DataRepository'

I18n.fallbacks = true;
I18n.defaultLocale = 'en';
I18n.translations = {

    "en-AU":en,
    "en-CA":en,
    "en-SG":en,
    "en-GB":en,
    "en-UK":en,
    "en-US":en,
    "en-CN":en,
    "en-HK":en,
    en,

    "zh-CN":zh,
    "zh-Hans-CN":zh,
    "zh-HK": zh_CHT,
    "zh-Hant-HK":zh_CHT,
    "zh-TW": zh_CHT,
    "zh-Hant-TW":zh_CHT,
    "zh-Hant-MO":zh_CHT,
    zh,

};

I18n.localeLanguage = () => {
    new DataRepository().fetchLocalRepository('localLanguage')
        .then((res)=>{
            I18n.locale = res;
        })
        .catch((error)=>{

        });
    return I18n.locale;
};
export { I18n, getLanguages };
export default I18n