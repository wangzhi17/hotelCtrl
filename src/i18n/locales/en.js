/**
 * Created by kukuchong on 2018/6/12.
 */
export default {

    "controllerUnconnected": "Not connected to gateway",
    "auto": "Auto",
    "off": "Off ",
    "on": "On",

    "light": "LIGHTING",
    "MainArea": "Main Area",
    "Bedroom": "Bedroom",
    "Master": "Master",
    "LeftReadingLight": "Reading Light",
    "RightReadingLight": "Reading Light",
    "NightLight": "Night Light",
    "Mirror": "Mirror",
    "Bathroom": "Bathroom",
    "Chandelier": "Chandelier",
    "LivingRoom": "Living Room",
    "LivingRoomTitle": "Living Room", //new field
    "BathroomTitle": "Bathroom", //new field
    "BedroomTitle": "Bedroom", //new field

    "air": "AIR CONDITIONER",
    "fan": "Fan", //new field
    "Temperature": "Temperature",
    "HeaterOn": "Heater",
    "FanSpeed": "Fan Speed",
    "Low": "Low",
    "Medium": "Medium",
    "High": "High",
    "airPower": "Please turn on the air conditioner",
    "minValue": "Reached the minimum degree",
    "maxValue": "Reached the maximum degree",
    "autoModeStatus": "Fan cannot be adjusted under auto mode"
}