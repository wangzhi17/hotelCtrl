/**
 * Created by kukuchong on 2018/6/12.
 */
export default {

    "controllerUnconnected": "未连接到网关",
    "auto": "自动模式",
    "off": "关",
    "on": "开",

    "light": "灯光",
    "MainArea": "主区",
    "Bedroom": "客房",
    "Master": "主控",
    "LeftReadingLight": "阅读灯",
    "RightReadingLight": "阅读灯",
    "NightLight": "夜灯",
    "Mirror": "镜灯",
    "Bathroom": "浴室",
    "Chandelier": "吊灯",
    "LivingRoom": "客厅",
    "LivingRoomTitle": "客厅", //new field
    "BathroomTitle": "浴室", //new field
    "BedroomTitle": "睡房", //new field

    "air": "空调",
    "fan": "送风", //new field
    "Temperature": "温度",
    "HeaterOn": "暖风",
    "FanSpeed": "风速",
    "Low": "弱风",
    "Medium": "中风",
    "High": "强风",
    "airPower": "请启动空调",
    "minValue": "空调已经调至最低温度",
    "maxValue": "空调已经调至最高温度",
    "autoModeStatus": "送风模式启动中, 无法调节温度"
}