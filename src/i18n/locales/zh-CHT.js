/**
 * Created by kukuchong on 2018/6/12.
 */
export default {

    "controllerUnconnected": "未連接到閘道",
    "auto": "自動模式",
    "off": "關",
    "on": "開",

    "light": "燈光",
    "MainArea": "主區",
    "Bedroom": "客房",
    "Master": "主控",
    "LeftReadingLight": "閱讀燈",
    "RightReadingLight": "閱讀燈",
    "NightLight": "夜燈",
    "Mirror": "鏡燈",
    "Bathroom": "浴室",
    "Chandelier": "吊燈",
    "LivingRoom": "客廳",
    "LivingRoomTitle": "客廳", //new field
    "BathroomTitle": "浴室", //new field
    "BedroomTitle": "睡房", //new field

    "air": "冷氣",
    "fan": "送風", //new field
    "Temperature": "溫度",
    "HeaterOn": "暖風",
    "FanSpeed": "風速",
    "Low": "弱風",
    "Medium": "中風",
    "High": "強風",
    "airPower": "請啟動冷氣",
    "minValue": "冷氣已經調至最低溫度",
    "maxValue": "冷氣已經調至最高溫度",
    "autoModeStatus": "送風模式啟動中, 無法調節溫度"
}