/**
 * Created by kukuchong on 2018/6/1.
 */
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {
    View,
    Text,
    StyleSheet,
    ImageBackground,
    Image,
    TouchableOpacity,
} from 'react-native'
import I18n from '../../i18n/index'
import Toast from "react-native-root-toast";
import DataRepository from './../../utils/DataRepository'
import writeLog from "../../utils/writeLog";

let timeOutHandle = null;
let timeOutCleanShowSetTime = null;

class AirCtrl extends Component {

    constructor(props) {
        super(props);
        this.state = {
            languageCode: this.props["languageCode"],
            onStartTemp10: 0,
        };

        this.temps = {
            16: {
                value: 16,
                setValue: '42733000',
                getValue: '42733000',

            },
            16.5: {
                value: 16.5,
                setValue: '4276c800',
                getValue: '4276c800',

            },
            17: {
                value: 17,
                setValue: '427a6000',
                getValue: '427a6000',

            },
            17.5: {
                value: 17.5,
                setValue: '427df800',
                getValue: '427df800',

            },
            18: {
                value: 18,
                setValue: '4280cc00',
                getValue: '4280cc00',

            },
            18.5: {
                value: 18.5,
                setValue: '42829800',
                getValue: '42829800',

            },
            19: {
                value: 19,
                setValue: '42846400',
                getValue: '42846400',

            },
            19.5: {
                value: 19.5,
                setValue: '42863000',
                getValue: '42863000',

            },
            20: {
                value: 20,
                setValue: '4287fc00',
                getValue: '4287fc00',

            },
            20.5: {
                value: 20.5,
                setValue: '4289cc00',
                getValue: '4289cc00',

            },
            21: {
                value: 21,
                setValue: '428b9800',
                getValue: '428b9800',

            },
            21.5: {
                value: 21.5,
                setValue: '428d6400',
                getValue: '428d6400',

            },
            22: {
                value: 22,
                setValue: '428f3000',
                getValue: '428f3000',

            },
            22.5: {
                value: 22.5,
                setValue: '4290fc00',
                getValue: '4290fc00',

            },
            23: {
                value: 23,
                setValue: '4292cc00',
                getValue: '4292cc00',

            },
            23.5: {
                value: 23.5,
                setValue: '42949800',
                getValue: '42949800',

            },
            24: {
                value: 24,
                setValue: '42966400',
                getValue: '42966400',

            },
            24.5: {
                value: 24.5,
                setValue: '42983000',
                getValue: '42983000',

            },
            25: {
                value: 25,
                setValue: '4299fc00',
                getValue: '4299fc00',

            },
            25.5: {
                value: 25.5,
                setValue: '429bcc00',
                getValue: '429bcc00',
            },
            26: {
                value: 26,
                setValue: '429d9800',
                getValue: '429d9800',

            },
            26.5: {
                value: 26.5,
                setValue: '429f6400',
                getValue: '429f6400',

            },
            27: {
                value: 27,
                setValue: '42a13000',
                getValue: '42a13000',

            },
            27.5: {
                value: 27.5,
                setValue: '42a2fc00',
                getValue: '42a2fc00',

            },
            28: {
                value: 28,
                setValue: '42a4cc00',
                getValue: '42a4cc00',

            }
        }
    }

    componentDidMount() {
        //this.getStatus()
    }


    componentWillMount() {
        new DataRepository().saveLocalRepository('localLanguage', I18n.locale, (error) => {
            if (error) {
                alert(error);
            }
        });
        this.refreshLanguage(this.state.languageCode);
    }

    refreshLanguage = (index) => {
        switch (index) {
            case 0:
                I18n.locale = 'en';
                break;
            case 1:
                I18n.locale = 'zh-CN';
                break;
            case 2:
                I18n.locale = 'zh-HK';
                break;
        }
        this.setState({
            localeLanguage: I18n.locale
        });

    };


    getStatus() {

        if (global.config && global.tcp.status) {
            global.config.air.row.map(rows => {

                    let power = global.config.device[rows.aid].power;
                    let powerKey = `${power.lid}.${power.rcuid}.${power.port}`;

                    let temperature = global.config.device[rows.aid].temperature;
                    let tempKey = `${temperature.lid}.${temperature.rcuid}.${temperature.port}`;

                    let speed = global.config.device[rows.aid].speed;
                    let speedKey = `${speed.lid}.${speed.rcuid}.${speed.port}`;

                    let mode = global.config.device[rows.aid].mode;
                    let modeKey = `${mode.lid}.${mode.rcuid}.${mode.port}`;
                    try {
                        let query = `GET;{${global.tcp.ip.split('.')[3]}.${powerKey}};\x0d\x0a`;
                        global.tcp.client.write(query);

                        query = `GET;{${global.tcp.ip.split('.')[3]}.${tempKey}};\x0d\x0a`;
                        global.tcp.client.write(query);

                        query = `GET;{${global.tcp.ip.split('.')[3]}.${speedKey}};\x0d\x0a`;
                        global.tcp.client.write(query);

                        query = `GET;{${global.tcp.ip.split('.')[3]}.${modeKey}};\x0d\x0a`;
                        global.tcp.client.write(query);
                        console.info('发送查询空调状态命令结束');

                    } catch (e) {

                    }
                }
            )
        }
    }

    controlSpeed = (value, speedKey, powerKey) => {
        if (global.tcp.status) {
            if (this.props.tcp[powerKey] === '3f800000') {
                let data = `SET;${value};{${global.tcp.ip.split('.')[3]}.${speedKey}};\x0d\x0a`;
                global.tcp.client.write(data)
                writeLog(data);
            } else {
                Toast.show(`${I18n.t('airPower')}`)
            }
        } else {
            Toast.show(`${I18n.t('controllerUnconnected')}`)
        }
    };

    controlPower = (value, stateKey) => {
        if (global.tcp.status) {
            switch (value) {
                case '3f800000':
                    value = '00000000';
                    break;
                case '00000000':
                    value = '3f800000';
                    break;
            }
            let data = `SET;${value};{${global.tcp.ip.split('.')[3]}.${stateKey}};\x0d\x0a`;
            global.tcp.client.write(data);
            writeLog(data);
        } else {
            Toast.show(`${I18n.t('controllerUnconnected')}`)
        }
    };

    controlTemperature = (tempKey, powerKey, modeStatus, value) => {

        if (global.tcp.status) {
            if (this.props.tcp[powerKey] === '3f800000') {
                let data = "";
                if (modeStatus === '40400000') {
                    if (value >= 16 && value <= 28) {
                        Object.keys(this.temps).map(key => {
                            let tempObject = this.temps[key];
                            if (value === tempObject.value) {
                                data = `SET;${tempObject.setValue};{${global.tcp.ip.split('.')[3]}.${tempKey}};\x0d\x0a`;

                                timeOutHandle = setTimeout(
                                    () => {
                                        global.tcp.client.write(data);
                                        writeLog(data);
                                        timeOutCleanShowSetTime = setTimeout(() => {
                                            this.updateTemp10(0);
                                            clearTimeout(timeOutCleanShowSetTime);
                                        }, 3000);
                                        clearTimeout(timeOutHandle);

                                    }, 1500);

                            }
                        });

                    }

                } else Toast.show(`${I18n.t('autoModeStatus')}`);

            } else {
                Toast.show(`${I18n.t('airPower')}`);
            }

        } else {
            Toast.show(`${I18n.t('controllerUnconnected')}`);
        }

    };

    controlMode = (value, modeKey, powerStatus) => {
        if (global.tcp.status) {
            if (powerStatus === '3f800000') {
                switch (value) {
                    case '40400000':
                        value = '00000000';
                        break;
                    case '00000000':
                        value = '40400000';
                        break;
                }
                let data = `SET;${value};{${global.tcp.ip.split('.')[3]}.${modeKey}};\x0d\x0a`;
                global.tcp.client.write(data);
                writeLog(data);
            } else Toast.show(`${I18n.t('airPower')}`);
        } else Toast.show(`${I18n.t('controllerUnconnected')}`);
    };

    setTemp = (powerKey, modeStatus, temp, flag) => {
        if (this.props.tcp[powerKey] === '3f800000' && modeStatus === '40400000') {
            temp = this.state.onStartTemp10 == 0 ? temp : this.state.onStartTemp10;
            if (timeOutHandle != null)
                clearTimeout(timeOutHandle);
            if (timeOutCleanShowSetTime != null)
                clearTimeout(timeOutCleanShowSetTime);
            if (flag)
                temp = temp + 0.5;
            else temp = temp - 0.5;

            if (temp < 16) {
                Toast.show(`${I18n.t('minValue')}`);
            } else if (temp > 28) {

                Toast.show(`${I18n.t('maxValue')}`);
            } else {
                this.updateTemp10(temp);
                console.info(`温度值：${temp}`)
            }

        }
    };
    getTemp = (tempStatus16F) => {
        let temp = 0;
        Object.keys(this.temps).map(key => {
            let tempObject = this.temps[key];

            if (tempStatus16F === tempObject.getValue) {
                temp = tempObject.value;

            }
        });
        return temp;
    };
    updateTemp10 = (temp) => {
        this.setState({
            onStartTemp10: temp
        });
    };

    render() {
        return (
            <View style={styles.content}>
                {
                    global.config && global.config.air.row.map((rows, index) => {

                        let power = global.config.device[rows.aid].power;
                        let powerKey = `${power.lid}.${power.rcuid}.${power.port}`;

                        let temperature = global.config.device[rows.aid].temperature;
                        let tempKey = `${temperature.lid}.${temperature.rcuid}.${temperature.port}`;

                        let speed = global.config.device[rows.aid].speed;
                        let speedKey = `${speed.lid}.${speed.rcuid}.${speed.port}`;

                        let mode = global.config.device[rows.aid].mode;
                        let modeKey = `${mode.lid}.${mode.rcuid}.${mode.port}`;

                        let powerStatus = this.props.tcp[powerKey];

                        let tempStatus16F = this.props.tcp[tempKey];

                        let tempStatus10 = this.getTemp(tempStatus16F);

                        if (tempStatus10 < 0)
                            tempStatus10 = 0;

                        let speedStatus = this.props.tcp[speedKey];

                        let modeStatus = this.props.tcp[modeKey];

                        return (
                            <View key={`row${index}`}>
                                <View style={styles.temperControlUnit}>
                                    <View style={styles.tempUnit}>
                                        <View style={{
                                            flexDirection: 'row',
                                            width: 400,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                            <Image style={{height: 24, width: 17}}
                                                   source={powerStatus === '3f800000' && modeStatus === '40400000' ? require('./img/Temperature.png') : require('./img/TemperatureOFF.png')}/>
                                            <Text
                                                style={powerStatus === '3f800000' && modeStatus === '40400000' ? styles.textON : styles.textOFF}>
                                                {I18n.t('Temperature')}
                                            </Text>

                                        </View>
                                        <View style={{flexDirection: 'row', top: 22}}>
                                            <TouchableOpacity style={{width: 84, height: 84}}
                                                              onPressIn={() => this.setTemp(powerKey, modeStatus, tempStatus10, false)}
                                                              onPress={() => this.controlTemperature(tempKey, powerKey, modeStatus, this.state.onStartTemp10)}>

                                                <Image style={{width: 84, height: 84}}
                                                       source={powerStatus === '3f800000' && modeStatus === '40400000' ? require('./img/Jian.png') : require('./img/JianOFF.png')}/>
                                            </TouchableOpacity>

                                            <ImageBackground style={styles.tempBG}
                                                             source={require('./img/TemperatureBG.png')}>
                                                <Text
                                                    style={powerStatus === '3f800000' && modeStatus === '40400000' ? styles.tempShowON : styles.tempShowOFF}>
                                                    {`${this.state.onStartTemp10 == 0 ? tempStatus10 : this.state.onStartTemp10}℃`}
                                                </Text>
                                            </ImageBackground>
                                            <TouchableOpacity style={{width: 84, height: 84}}
                                                              onPressIn={() => this.setTemp(powerKey, modeStatus, tempStatus10, true)}
                                                              onPressOut={() => this.controlTemperature(tempKey, powerKey, modeStatus, this.state.onStartTemp10)}>
                                                <Image style={{width: 84, height: 84, marginLeft: 60}}
                                                       source={powerStatus === '3f800000' && modeStatus === '40400000' ? require('./img/Jia.png') : require('./img/JiaOFF.png')}/>
                                            </TouchableOpacity>

                                        </View>
                                        <View style={{flexDirection: 'row', top: 28}}>

                                        </View>
                                    </View>

                                    <View style={styles.controlUnit}>
                                        <TouchableOpacity onPress={() => this.controlPower(powerStatus, powerKey)}>
                                            <Image style={{width: 100, height: 54}}
                                                   source={powerStatus === '3f800000' ? require('./img/on.png') : require('./img/off.png')}/>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => this.controlMode(modeStatus, modeKey, powerStatus)}>
                                            <Image style={{top: 10, width: 110, height: 54}}
                                                   source={modeStatus === '40400000' ? (this.state.languageCode == 0 ? require('./img/autoModeEN.png') : (this.state.languageCode == 1 ? require('./img/autoModeCN.png') : require('./img/autoModeCHT.png'))) : (this.state.languageCode == 0 ? require('./img/fanModeEN.png') : (this.state.languageCode == 1 ? require('./img/fanModeCN.png') : require('./img/fanModeCHT.png')))}/>
                                        </TouchableOpacity>

                                    </View>

                                </View>

                                <View style={styles.speedWrapUnit}>
                                    <View style={styles.speedIconUnit}>
                                        <Image style={{width: 28, height: 24}}
                                               source={powerStatus === '3f800000' ? require('./img/FanSpeedON.png') : require('./img/FanSpeedOFF.png')}/>
                                        <Text style={powerStatus === '3f800000' ? styles.textON : styles.textOFF}>
                                            {I18n.t('FanSpeed')}
                                        </Text>
                                    </View>

                                    <View style={styles.speedUnit}>
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => this.controlSpeed('3f800000', speedKey, powerKey)}>
                                                <Image style={{width: 86, height: 86}}
                                                       source={powerStatus === '3f800000' ? (speedStatus === '3f800000' ? require('./img/LowON.png') : require('./img/LowOFF.png')) : require('./img/LowOFF.png')}/>

                                            </TouchableOpacity>
                                            <Text
                                                style={powerStatus === '3f800000' ? (speedStatus === '3f800000' ? styles.speedTextActive : styles.speedTextON) : styles.speedTextOFF}>
                                                {I18n.t('Low')}
                                            </Text>
                                        </View>

                                        <View>
                                            <TouchableOpacity style={{marginLeft: 100, marginRight: 100}}
                                                              onPress={() => this.controlSpeed('40000000', speedKey, powerKey)}>
                                                <Image style={{width: 86, height: 86}}
                                                       source={powerStatus === '3f800000' ? (speedStatus === '40000000' ? require('./img/MediumON.png') : require('./img/MediumOFF.png')) : require('./img/MediumOFF.png')}/>

                                            </TouchableOpacity>
                                            <Text
                                                style={powerStatus === '3f800000' ? (speedStatus === '40000000' ? styles.speedTextActive : styles.speedTextON) : styles.speedTextOFF}>
                                                {I18n.t('Medium')}
                                            </Text>
                                        </View>

                                        <View>
                                            <TouchableOpacity
                                                onPress={() => this.controlSpeed('40400000', speedKey, powerKey)}>
                                                <Image style={{width: 86, height: 86}}
                                                       source={powerStatus === '3f800000' ? (speedStatus === '40400000' ? require('./img/HighON.png') : require('./img/HighOFF.png')) : require('./img/HighOFF.png')}/>

                                            </TouchableOpacity>
                                            <Text
                                                style={powerStatus === '3f800000' ? (speedStatus === '40400000' ? styles.speedTextActive : styles.speedTextON) : styles.speedTextOFF}>
                                                {I18n.t('High')}
                                            </Text>
                                        </View>
                                    </View>

                                </View>
                            </View>
                        )
                    })
                }
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        tcp: state.air
    }
}

export default connect(mapStateToProps)(AirCtrl)
const styles = StyleSheet.create({
    content: {
        flex: 1,
        height: 500,
        width: 984,
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center'
    },
    temperControlUnit: {
        height: 293,
        width: 984,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    tempUnit: {
        height: 293,
        width: 488,
        left: 44,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tempBG: {
        width: 200,
        height: 84,
        marginLeft: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    controlUnit: {
        top: -40,
        left: 108,
        alignItems: 'center',
        justifyContent: 'center'
    },
    speedWrapUnit: {
        top: 40.5,
        height: 219,
        width: 984
    },
    speedIconUnit: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    speedUnit: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        top: 32
    },
    tempShowON: {
        fontSize: 44,
        color: '#221E1F'
    },
    tempShowOFF: {
        fontSize: 44,
        color: '#D0D2D3'
    },
    speedTextActive: {
        fontSize: 20,
        color: '#5E9732',
        textAlign: 'center'
    },
    speedTextON: {
        fontSize: 20,
        color: '#221E1F',
        textAlign: 'center'
    },
    speedTextOFF: {
        fontSize: 20,
        color: '#716F73',
        textAlign: 'center'
    },
    textON: {
        left: 10,
        fontSize: 18,
        color: '#221E1F'
    },
    textOFF: {
        left: 10,
        fontSize: 18,
        color: '#716F73'
    },

});
