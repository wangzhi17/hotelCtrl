import React, {Component} from 'react'

import {AppState, View} from 'react-native'
import {connect} from 'react-redux'
import getConfigFile from './../../utils/getConfigFile'
import tcpClientIOS from './../../utils/tcpClient';
import ConnTcp from "../../utils/tcp";
import writeLog from "../../utils/writeLog";
class Tcp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appState: AppState.currentState
        };

    }

    componentWillMount() {
        getConfigFile(this.props["isFirst"], JSON.parse(this.props["config"]));
    }

    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);

    }


    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            writeLog("App 处于前台");
        }
        this.setState({
            appState: nextAppState
        });

        switch (this.state.appState) {
            case 'inactive':
                writeLog("切换应用");
                break;

            case 'background':
                //console.info('后台运行APP');
                if (global.tcp.status) {
                    global.tcp.client.end();
                    global.tcp.status = false;
                    //console.info('end rcu连接');
                }
                if (global.tcp.statusIOS) {
                    global.tcp.clientIOS.end();
                    global.tcp.statusIOS = false;
                    //console.info('end 本地连接');
                }

                break;

            case 'active':
                if ((!global.tcp.status) && this.state.appState === 'active') {

                    ConnTcp(global.tcp.ip, 6003);
                    writeLog('连接控制器');
                }
                if ((!global.tcp.statusIOS) && this.state.appState === 'active') {

                    tcpClientIOS(61000);//连接原生端
                    writeLog('连接本地tcp服务器');

                }
                break;
        }

    };

    render() {
        return (
            <View>

            </View>
        )
    }

}

function mapStateToProps(state) {
    return {
        tcp: state.tcp
    }
}

export default connect(mapStateToProps)(Tcp)