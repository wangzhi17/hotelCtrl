/**
 * Created by kukuchong on 2018/6/13.
 */
export default (name) => {
    let src = null;
    switch (name) {
        case 'Master':
            src = require('./img/Master1.png');
            break;
        case 'LeftReadingLight':
            src = require('./img/LeftReadingLightON.png');
            break;
        case 'RightReadingLight':
            src = require('./img/RightReadingLightON.png');
            break;
        case 'NightLight':
            src = require('./img/NightLight1.png');
            break;
        case 'Mirror':
            src = require('./img/Mirror1.png');
            break;
        case 'Bathroom':
            src = require('./img/Bathroom1.png');
            break;

    }
    return src;
}