import React, {Component} from 'react'

import {
    View,
    Text,
    StyleSheet,
    Image,
    ImageBackground,
} from 'react-native'
import {connect} from 'react-redux'
import Light from './light'
import I18n from "../../i18n";
import DataRepository from './../../utils/DataRepository'

class LightCtrl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            languageCode: this.props["languageCode"],
        };
    }

    componentWillMount() {
        new DataRepository().saveLocalRepository('localLanguage', I18n.locale, (error) => {
            if (error) {
                alert(error);
            }
        });
        this.refreshLanguage(this.state.languageCode);
    }

    componentDidMount() {
        //this.getStatus()

    }

    componentWillUnmount() {

    }

    getStatus() {
        if (global.config && global.tcp.status) {
            global.config.light.room.map(room => {
                room.row.map(row => {
                    row.col.map(col => {
                        let device = global.config.device[col.id];
                        let key = `${device.lid}.${device.rcuid}.${device.port}`;
                        let query = `GET;{${global.tcp.ip.split('.')[3]}.${key}};\x0d\x0a`;
                        global.tcp.client.write(query)

                    })
                })
                console.info('发送查询灯光状态命令结束');
            });

        }
    }


    refreshLanguage = (index) => {
        switch (index) {
            case 0:
                I18n.locale = 'en';
                break;
            case 1:
                I18n.locale = 'zh-CN';
                break;
            case 2:
                I18n.locale = 'zh-HK';
                break;
        }
        this.setState({
            localeLanguage: I18n.locale
        });

    };

    render() {
        return (
            <ImageBackground style={styles.content} source={require('./img/SingleRoom.png')}>
                {
                    global.config && global.config.light.room.map((rooms, roomsindex) => {
                        if (rooms.name === 'MainArea') {
                            return (
                                <View style={styles.MainAreaContent} key={`${roomsindex}`}>
                                    <View style={styles.MainAreaTitle}>
                                        <Image style={{width: 17.2, height: 17.2}}
                                               source={require('./img/MainArea.png')}/>
                                        <Text style={styles.TitleText}>
                                            {I18n.t('MainArea')}
                                        </Text>
                                    </View>
                                    <View style={{flex: 1, alignItems: 'center', top:28}}>
                                        {
                                            rooms.row.map((rows, rowsindex) => {
                                                return (
                                                    <View key={`${rowsindex}`} style={styles.rowsWrap}>
                                                        {
                                                            rows.col.map(cols => {
                                                                let device = global.config.device[cols.id];
                                                                let key = `${device.lid}.${device.rcuid}.${device.port}`;
                                                                let active = this.props.tcp[key];

                                                                return (
                                                                    <Light key={key} stateKey={key}
                                                                           active={active}
                                                                           light={device} port={device.port}/>
                                                                )

                                                            })
                                                        }
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                </View>
                            )
                        }
                        if (rooms.name === 'BathRoom') {
                            return (
                                <View style={styles.BathRoomContent} key={`${roomsindex}`}>

                                    <View style={styles.BathRoomTitle}>
                                        <Image style={{width: 24, height: 26}}
                                               source={require('./img/Bathroom_title.png')}/>
                                        <Text style={styles.TitleText}>
                                            {I18n.t('Bathroom')}
                                        </Text>
                                    </View>
                                    <View style={{flex: 1, alignItems: 'center', top: 52}}>
                                        {
                                            rooms.row.map((rows, rowsindex) => {
                                                return (
                                                    <View key={`${rowsindex}`} style={styles.rowsWrap}>
                                                        {
                                                            rows.col.map(cols => {
                                                                let device = global.config.device[cols.id];
                                                                let key = `${device.lid}.${device.rcuid}.${device.port}`;
                                                                let active = this.props.tcp[key];
                                                                return (
                                                                    <Light key={key} stateKey={key}
                                                                           active={active}
                                                                           light={device} port={device.port}/>
                                                                )

                                                            })
                                                        }
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                </View>
                            )
                        }
                    })
                }
            </ImageBackground>

        )
    }
}


function mapStateToProps(state) {
    return {
        tcp: state.light
    }
}

export default connect(mapStateToProps)(LightCtrl)
const styles = StyleSheet.create({
    content: {
        //flex: 1,
        width: 984,
        height: 500,
        position: 'relative',
        flexDirection: 'row',
    },
    MainAreaContent: {
        width: 528,
        height: 470,
        marginTop: 20,
        marginLeft: 20
    },
    MainAreaTitle: {
        width: 102.8,
        height: 24,
        left: 223.2,
        top: 16,
        position: 'absolute',
        flexDirection: 'row'
    },
    TitleText: {
        fontSize: 16,
        color: '#221E1F',
        marginLeft: 10.6
    },
    BathRoomContent: {
        width: 386,
        height: 470,
        marginTop: 20,
        marginLeft: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    BathRoomTitle: {
        width: 386,
        height: 44,
        left: 30,
        right: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    rowsWrap: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});