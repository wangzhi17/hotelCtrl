import React, {Component} from 'react'
import {View, Image, TouchableOpacity, Text, StyleSheet} from 'react-native'
import imgSrc from './imgSrc'
import I18n from '../../i18n/index'
import Toast from 'react-native-root-toast';
import writeLog from "../../utils/writeLog";

export default class Light extends Component {
    constructor(props) {
        super(props)
    }

    control = () => {
        if (global.tcp.status) {
            let data = `SET;0000000${+this.props.active};{${global.tcp.ip.split('.')[3]}.${this.props.stateKey}};\x0d\x0a`;
            try {
                global.tcp.client.write(data);
                writeLog(data);
            } catch (e) {
                writeLog(e.message);
            }

        } else {
            Toast.show(`${I18n.t('controllerUnconnected')}`)
        }
    }

    render() {
        const {active, light} = this.props;
        return (
            <View style={styles.lightWrap}>

                <TouchableOpacity style={styles.lightImgWrap} onPress={this.control}>
                    <Image style={styles.lightImg}
                           source={imgSrc(light.name)}/>

                </TouchableOpacity>
                <Text style={styles.lightText}>{I18n.t(light.name)}</Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    lightWrap: {
        //width: px2dp(260),
        marginLeft: 22.5,

        marginTop: 78,
        width: 152,
        alignItems: 'center',
        justifyContent: 'center'
    },
    lightImgWrap: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    lightImgWrapActive: {
        backgroundColor: "#FBFE0B"
    },
    lightImg: {
        width: 80,
        height: 80,
        backgroundColor: 'transparent',
    },
    lightText: {
        fontSize: 16,
        color: '#221E1F',
        textAlign: 'center'
    }
})