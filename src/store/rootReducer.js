/**
 * Created by kukuchong on 2018/6/17.
 */
import {INIT_STATE, UPDATE_STATE} from './actions'
import writeLog from "../utils/writeLog";
const initialState = {
    light: {},
    air: {},
    tcp: {},
    globalIndex: {}
}
export default function (state = initialState, action) {
    switch (action.type) {
        case INIT_STATE:
            if (!Object.keys(action.config).length) return state;
            //初始化灯的状态key
            action.config.light.room.map(room => {
                room.row.map(row => {
                    row.col.map(col => {
                        let device = action.config.device[col.id];
                        let key = `${device.lid}.${device.rcuid}.${device.port}`;
                        state.light[key] = +device['value'];
                        state.globalIndex[key] = "light";
                    })
                })
            });
            let device = action.config.device["MakeupRoom"];
            let key = `${device.lid}.${device.rcuid}.${device.port}`;
            state.light[key] = +device['value'];
            state.globalIndex[key] = "light";

            device = action.config.device["DoNotDisturb"];
            key = `${device.lid}.${device.rcuid}.${device.port}`;
            state.light[key] = +device['value'];
            state.globalIndex[key] = "light";

            //初始化空调的状态key
            action.config.air.row.map(row => {
                let currentTemp = action.config.device[row.aid].currentTemp;
                let currentTempKey = `${currentTemp.lid}.${currentTemp.rcuid}.${currentTemp.port}`;

                let power = action.config.device[row.aid].power;
                let powerKey = `${power.lid}.${power.rcuid}.${power.port}`;

                let mode = action.config.device[row.aid].mode;
                let modeKey = `${mode.lid}.${mode.rcuid}.${mode.port}`;

                let temperature = action.config.device[row.aid].temperature;
                let tempKey = `${temperature.lid}.${temperature.rcuid}.${temperature.port}`;

                let speed = action.config.device[row.aid].speed;
                let speedKey = `${speed.lid}.${speed.rcuid}.${speed.port}`;

                state.air[currentTempKey] = +power.value;
                state.air[powerKey] = +power.value;
                state.air[modeKey] = +power.value;
                state.air[speedKey] = +speed.value;
                state.air[tempKey] = +temperature.value;

                state.globalIndex[currentTempKey] = "air";
                state.globalIndex[powerKey] = "air";
                state.globalIndex[modeKey] = "air";
                state.globalIndex[speedKey] = "air";
                state.globalIndex[tempKey] = "air";

                }
            );
            return state;
        case UPDATE_STATE:
            if (action.data.indexOf('FB') == -1) return state;
            try {
                //console.info("data：", action.data);
                let key = action.data.split(';')[2].split(`${global.tcp.ip.split('.')[3]}.`)[1].split('}')[0];
                //console.info('data长度：',action.data.length);
                let value = action.data.split(';')[1];
                if (value === 'ffffffff')
                    value = '00000000';
                state[state.globalIndex[key]][key] = value;
                //console.info('数值：', value);
                state[state.globalIndex[key]] = Object.assign({}, state[state.globalIndex[key]]);
                if (key === '0.65.4' && global.tcp.statusIOS) {
                    let data = {"lightName": "MakeupRoom", "value": `MakeupRoom${value}}`};
                    global.tcp.clientIOS.write(JSON.stringify(data));

                }
                if (key === '0.65.3' && global.tcp.statusIOS) {
                    let data = {"lightName": "DoNotDisturb", "value": `DoNotDisturb${value}}`};
                    global.tcp.clientIOS.write(JSON.stringify(data));

                }return Object.assign({}, state)

            } catch (err) {
                console.error(err.message);
                writeLog(err.message);
                return state
            }
        default:
            return state;
    }

}