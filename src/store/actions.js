/**
 * Created by kukuchong on 2018/7/10.
 */
import readStatus from './../utils/readStatus'
import writeLog from "../utils/writeLog";
export const INIT_STATE = "INIT_STATE";
export const UPDATE_STATE = "UPDATE_STATE";


export function initState(config) {
    return {
        type: INIT_STATE,
        config
    }
}

export function updateState(data) {
    return {
        type: UPDATE_STATE,
        data
    }
}

export function getAllState() {
    return function (dispatch, getState) {
        if (global.tcp.status) {

            let {light, air} = getState();
            let query = '';
            //查询灯的状态
            for (let key in light) {
                query = `GET;{${global.tcp.ip.split('.')[3]}.${key}};\x0d\x0a`;
                global.tcp.client.write(query);
                writeLog(query);
            }

            //查询空调状态
            for (let key in air) {
                query = `GET;{${global.tcp.ip.split('.')[3]}.${key}};\x0d\x0a`;
                global.tcp.client.write(query)
                writeLog(query);
            }
            //清扫房间

            readStatus("MakeupRoom");

            //免打扰
            readStatus("DoNotDisturb");


            //其他设备
        }
    }
}
